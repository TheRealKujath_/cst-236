<?php
require_once "../PHP_Files/_autoloader.php";
/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 9/20/17
 * Time: 7:55 AM
 */
class Security
{

    private $email;
    private $password;
    private $db;



    public function __construct($email, $password)
    {
        $this->email=$email;
        $this->password=$password;
        $this->db = new Database();

    }


    public function login($email, $password){
        if($this->db->checkCredentials($email, $password)) {
            return true;
        }
    }


}