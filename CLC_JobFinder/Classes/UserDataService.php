<?php
require_once "../PHP_Files/_autoloader.php";
/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 11/3/17
 * Time: 9:07 AM
 */
class UserDataService
{

    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function createUser($firstName, $lastName, $email, $password, $ccNumber, $cHName, $csv, $exMonth, $exYear)
    {
        $sql = "INSERT INTO USERS (FIRSTNAME, LASTNAME, EMAIL, PASSWORD, CARD_NUM, CARD_HOLDER_NAME, CSV, EXPIRATION_MONTH, EXPIRATION_YEAR)
                VALUES ('" . $firstName . "' , '" . $lastName . "' , '" . $email . "' , '" . $password . "' , 
                '" . $ccNumber . "' , '" . $cHName . "' , '" . $csv . "' , '" . $exMonth . "' , '" . $exYear . "')";
        if ($this->db->getConn()->query($sql) == TRUE) {
            //echo "You are now registered.";
            return true;

        } else {
            echo "Error entering user info";
            return false;
        }

    }

    public function checkCredentials($email, $password)
    {

        $sql = "SELECT ID, EMAIL, PASSWORD
                FROM USERS WHERE " . " BINARY EMAIL='" . $email . "' AND " . " BINARY PASSWORD='" . $password . "'";
        $result = $this->db->getConn()->query($sql);
        if ($this->db->getConn()->error) {
            //echo "Connection failed";
            return false;
        } elseif ($result->num_rows == 1) {
            //echo "Login Successful";
            return true;
        } elseif ($result->num_rows == 0) {
            //echo "User does not exist";
            return false;
        } elseif ($result->num_rows > 1) {
            //echo "More than one user registered";
            return false;
        } else {
            //echo "Login failed";
            return false;
        }
    }

    public function enterCreditCard($ccNumber, $cHName, $csv, $exMonth, $exYear){
        $sql = "INSERT INTO CREDIT_CARD (CARD_NUM, CARD_HOLDER_NAME, CSV, EXPIRATION_MONTH, EXPIRATION_YEAR)
                VALUES ('" . $ccNumber . "' , '" . $cHName . "' , '" . $csv . "' , '" . $exMonth . "' , '" . $exYear . "')
                WHERE USER.ID = USER.getId()";
        if ($this->db->getConn()->query($sql) == TRUE) {
            //echo "You are now registered.";
            return true;
        } else {
            echo "Error entering cc info";
            return false;
        }
    }
}