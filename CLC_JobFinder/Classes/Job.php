<?php
require_once "../PHP_Files/_autoloader.php";
/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 9/21/17
 * Time: 2:57 PM
 */
class Job
{
    private $id;
    private $name;
    private $description;
    private $image;
    private $type;




    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }



    /**
     * Job constructor.
     * @param $name
     * @param $description
     * @param $contact
     */
    public function __construct($id, $name, $description, $image, $type)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->image = $image;
        $this->type = $type;

    }

/*
    public function getJobs(){
        $sql = "SELECT * FROM JOBS";
        $result = $this->conn->query($sql);
        $jobs = array();
        $index = 0;
        while($row = $result->fetch_assoc()){
            $id = $row["JOB_ID"];
            $name = $row["JOB_NAME"];
            $description = $row["JOB_DESCRIPTION"];
            $image = $row["JOB_IMAGE"];
            $job = new Job($id, $name, $description, $image);
            $jobs[$index] = $job;
            $index++;
        }
        $this->conn->close();
        return $jobs;
    }

    public function createJob($jobName, $jobDescription, $jobImage){
        $sql = "INSERT INTO JOBS (JOB_NAME, JOB_DESC, JOB_IMAGE)
                VALUES ('" . $jobName . "' , '" . $jobDescription . "' , '" . $jobImage . "')";
        if ($this->conn->query($sql) == TRUE) {
            return true;
        } else {
            return false;
        }
    }

    public function getJobById($id){
        $sql = "SELECT * FROM JOBS WHERE JOB_ID=" . $id ;
        $result = $this->conn->query($sql);
        if($result->num_rows==1){
            $row = $result->fetch_assoc();
            $job = new Job($row["JOB_ID"], $row["JOB_NAME"], $row["JOB_DESCRIPTION"], $row["JOB_IMAGE"]);

            return $job;
        }

        return null;
    }
*/
}