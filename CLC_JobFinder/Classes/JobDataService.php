<?php
require_once "../PHP_Files/_autoloader.php";
/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 11/3/17
 * Time: 9:16 AM
 */
class JobDataService
{

    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function getAllJobs(){
        $sql = "SELECT * FROM JOBS";
        $result = $this->db->getConn()->query($sql);
        $jobs = array();
        $index = 0;
        while($row = $result->fetch_assoc()){
            $id = $row["JOB_ID"];
            $name = $row["JOB_NAME"];
            $description = $row["JOB_DESCRIPTION"];
            $image = $row["JOB_IMAGE"];
            $type = $row["JOB_TYPE"];
            $job = new Job($id, $name, $description, $image, $type);
            $jobs[$index] = $job;
            $index++;
        }
        $this->db->closeConn();
        return $jobs;
    }

    public function createJob($jobName, $jobDescription, $jobImage, $jobType){
        $sql = "INSERT INTO JOBS (JOB_NAME, JOB_DESCRIPTION, JOB_IMAGE, JOB_TYPE)
                VALUES ('" . $jobName . "' , '" . $jobDescription . "' , '" . $jobImage . "' , '" . $jobType . "')";
        if ($this->db->getConn()->query($sql) == TRUE) {
            return true;
        } else {
            return false;
        }
    }

    public function getJobById($id){
        $sql = "SELECT * FROM JOBS WHERE JOB_ID=" . $id ;
        $result = $this->db->getConn()->query($sql);
        if($result->num_rows==1){
            $row = $result->fetch_assoc();
            $job = new Job($row["JOB_ID"], $row["JOB_NAME"], $row["JOB_DESCRIPTION"], $row["JOB_IMAGE"], $row["JOB_TYPE"]);

            return $job;
        }

        return null;
    }

    public function updateJobById($jobId, $jobName, $jobDescription, $jobImage, $jobType){
        $sql =  "UPDATE JOBS SET JOB_NAME = " . $jobName . ", JOB_DESCRIPTION = " . $jobDescription . "
        , JOB_IMAGE = " . $jobImage . ", JOB_TYPE = " . $jobType . " WHERE JOB_ID = " . $jobId ;
        $result = $this->db->getConn()->query($sql);
        if(mysqli_affected_rows($this->db->getConn()->$result) > 0){
            return true;
        }
        else
            return false;
    }

    public function deleteJobById($id){
        $sql = "DELETE FROM JOBS WHERE JOB_ID = " . $id ;
        $result = $this->db->getConn()->query($sql);
        if(mysqli_affected_rows($this->db->getConn()->$result) > 0){
            echo "TRUE";
            return true;
        }
        else {
            echo "FALSE";
            return false;
        }
    }
}