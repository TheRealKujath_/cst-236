<?php
/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 9/10/17
 * Time: 4:28 PM
 */
session_start();
require_once "../PHP_Files/_autoloader.php";

$email = $_POST["email"];
$password = $_POST["password"];

//validate data
if($email == NULL || trim($email) == "")
{
    $message = "Email required";
    include "../PHP_Files/_error.php";
}
elseif($password == NULL || trim($password) == "")
{
    $message = "Password required";
    include "../PHP_Files/_error.php";
}

else {
    $service = new UserDataService();
    if($service->checkCredentials($email, $password))
    {
        $_SESSION["principle"] = true;
        $message= "Login Successful";
        include "../PHP_Files/_error.php";
    }
    else {
        $message="Login Failed";
        include "../PHP_Files/_error.php";
    }
}
?>
