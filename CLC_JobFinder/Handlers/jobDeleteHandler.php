<?php
/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 11/6/17
 * Time: 12:45 AM
 */
session_start();
require_once "../PHP_Files/_autoloader.php";

$service = new JobDataService();
$id = $_GET["id"];

if($service->deleteJobById($id)){
    $message = "Job deleted successfully";
    include "../PHP_Files/_error.php";
}
else{
    $message = "Job not deleted successfully";
    include "../PHP_Files/_error.php";
}
