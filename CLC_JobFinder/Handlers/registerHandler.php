<?php

session_start();
require_once "../PHP_Files/_autoloader.php";



    //form data
    $firstName = $_POST["firstName"];
    $lastName = $_POST["lastName"];
    $email = $_POST["email"];
    $password = $_POST["password"];
    $ccNumber = $_POST["creditCardNumber"];
    $cHName = $_POST["cardHolderName"];
    $csv = $_POST["csv"];
    $exMonth = $_POST["exMonth"];
    $exYear = $_POST["exYear"];

//make sure posted form data is valid before inserting into the database

if ($firstName == NULL || trim($firstName) == "" || strlen($firstName) < 2 || strlen($firstName) > 45) {
    $message="First name is required";
    include "../PHP_Files/_error.php";
}
elseif ($lastName == NULL || trim($lastName) == "" || strlen($lastName) < 2 || strlen($lastName) > 45) {
    $message="Last name is required";
    include "../PHP_Files/_error.php";
}
elseif($email == NULL || trim($email) == "" || strlen($email) < 5 || strlen($email) > 50){
    $message="Email is required";
    include "../PHP_Files/_error.php";
}
elseif($password == NULL || trim($password) == "" || strlen($password) < 8 || strlen($password) > 50){
    $message="Password is required";
    include "../PHP_Files/_error.php";
}
elseif($ccNumber == NULL || trim($ccNumber) == "" || strlen($ccNumber) < 16 || strlen($ccNumber) > 16){
    $message="Credit card number is required";
    include "../PHP_Files/_error.php";
}
elseif($cHName == NULL || trim($cHName) == "" || strlen($cHName) < 4 || strlen($cHName) > 100){
    $message="Card holder name is required";
    include "../PHP_Files/_error.php";
}
elseif($csv == NULL || trim($csv) == "" || strlen($csv) < 3 || strlen($csv) > 3){
    $message="CSV is required";
    include "../PHP_Files/_error.php";
}
elseif($exMonth == NULL || trim($exMonth) == "" || strlen($exMonth) < 2 || strlen($exMonth) > 2){
    $message="Expiration month is required";
    include "../PHP_Files/_error.php";
}
elseif($exYear == NULL || trim($exYear) == "" || strlen($exYear) < 2 || strlen($exYear) > 2){
    $message="Expiration year is required";
    include "../PHP_Files/_error.php";
}

else {
    //connect to database and input a user

    $service = new UserDataService();

    if ($service->createUser($firstName, $lastName, $email, $password, $ccNumber, $cHName, $csv, $exMonth, $exYear)) {
        $message = "User created successfully";
        include "../PHP_Files/_error.php";

        /*if($service->enterCreditCard($ccNumber, $cHName, $csv, $exMonth, $exYear)) {
            //header("Location: login.php");
            //$message = "Registration Successful";
            //include "../PHP_Files/_error.php";
        }
        else {
            $message = "CC entry failed";
            //include "../PHP_Files/_error.php";
        }*/
    } else {
        $message = "Registration failed at User creation";
        include "../PHP_Files/_error.php";
    }
    //$service->$this->db->closeConn();
}

?>

