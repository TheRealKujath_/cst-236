<?php
/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 11/5/17
 * Time: 11:48 PM
 */

session_start();
require_once "../PHP_Files/_autoloader.php";

$jobName = $_POST["jobName"];
$jobDescription = $_POST["jobDescription"];
$jobImage = $_POST["imageSource"];
$jobType = $_POST["jobType"];
$jobId = $_GET["id"];

$service = new JobDataService();
if($service->createJob($jobName, $jobDescription, $jobImage, $jobType)) {
    $message= "Job Update Successful";
    include "../PHP_Files/_error.php";
}
else {
    $message="Job Update failed";
    include "../PHP_Files/_error.php";
}