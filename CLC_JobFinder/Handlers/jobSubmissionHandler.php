<?php
/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 9/24/17
 * Time: 8:32 PM
 */
session_start();
require_once "../PHP_Files/_autoloader.php";

    $jobName = $_POST["jobName"];
    $jobDescription = $_POST["jobDescription"];
    $jobImage = $_POST["imageSource"];
    $jobType = $_POST["jobType"];

//make sure posted form data is valid before inserting into the database
if ($jobName == NULL || trim($jobName) == "") {
    $message="Job name is required";
    include "../PHP_Files/_error.php";
}
elseif ($jobDescription == NULL || trim($jobDescription) == "") {
    $message="Job description is required";
    include "../PHP_Files/_error.php";
}
elseif ($jobImage == NULL || trim($jobImage) == "") {
    $message="Job image source is required";
    include "../PHP_Files/_error.php";
}
elseif ($jobType == NULL) {
    $message="Job type is required";
    include "../PHP_Files/_error.php";
}
else{
//Input job to database
$service = new JobDataService();
if($service->createJob($jobName, $jobDescription, $jobImage, $jobType)) {
    //header("Location: login.php");
    $message= "Job Creation Successful";
    include "../PHP_Files/_error.php";
}
else {
    $message="Job Creation failed";
    include "../PHP_Files/_error.php";
}
}

