<?php
/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 9/20/17
 * Time: 8:03 AM
 */
session_start();

?>
<style>
    <?php
        include "../CSS/pageStyle.css";
    ?>
</style>

<ul>
    <li><a href="../PHP_Files/index.php">Home</a></li>
    <?php
    if(isset($_SESSION["principle"])==false || $_SESSION["principle"]==null || $_SESSION["principle"]==false){
        ?>
        <li><a href="../PHP_Files/login.php">Login</a></li>
        <li><a href="../PHP_Files/register.php">Register</a></li>

        <?php
        }
    else {
        ?>

        <li><a href="../PHP_Files/jobListPage.php">Job List</a></li>
        <li><a href="../PHP_Files/jobSubmit.php">Add a Job</a></li>
        <li><a href="../PHP_Files/watchList.php">Submit a Resume</a></li>
        <li><a href="../PHP_Files/logout.php">Logout</a></li>

        <?php
    }
    ?>
</ul>
