<?php
/**
 * Created by PhpStorm.
 * User: natelee
 * Date: 9/24/17
 * Time: 8:31 PM
 */

include "_header.php";
require_once "_autoloader.php";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>New Job</title>
    <link rel="stylesheet" href="../CSS/pageStyle.css">
</head>
<body align="center" bgcolor="#f0f8ff">

<form action="../Handlers/jobSubmissionHandler.php" method="POST">
    <table align="center">
        <tr>
            <td>Job Name:</td>
            <td><input type="text" name="jobName" required minlength="5" maxlength="45"/></td>
        </tr>
        <tr>
            <td>Description:</td>
            <td><textarea name="jobDescription" rows="5" cols="100" minlength="10" maxlength="1000" required></textarea></td>
        </tr>
        <tr>
            <td>Image url:</td>
            <td><input type="url" name="imageSource" required maxlength="500"/></td>
        </tr>
        <tr>
            <td>Job Type:</td>
            <td><input type="radio" name="jobType" value="partTime"/>Part time
            <input type="radio" name="jobType" value="fullTime"/>Full time
            <input type="radio" name="jobType" value="freelance"/>Freelance</td>
        </tr>
        <tr>
            <td><input type="submit" name="submit"/></td>
        </tr>
    </table>
</form>
<br>


</body>
</html>